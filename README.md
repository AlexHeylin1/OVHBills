
# OVHBills - Get your OVH bills the easy way

OVHBills allows you to get your OVH bills the easy way:
* PDF file for each bill is downloaded to the path given in config file.
* Details of each bill is recorder in two tables of the database (bills and billsDetails).

Set the date from which you want to get the bills in config.php.
However, OVHBills is just getting the new bills.
For example, if you are running the script every night in a CRON task, you can put "3 days ago" : if for some reason the script doesn't run one night, you are not going to miss anything.

## What is the script doing?

* Creating the tables ***bills*** and ***billsDetails*** if they don't exist,
* Listing all the bills,
* Getting them as PDF files,
* Getting the details of each bills in the tables,
* Setting the amount of "deposit" for each bill,
* Setting the amount of "outConsumption" for each bill.

## PHP wrapper of OVH API

The current work is based on the lightweight PHP wrapper for OVH APIs,
that you should install first.

Learn more at: https://github.com/ovh/php-ovh

## Usage:

```
$bills = new OVHBills(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
$bills->run();
```

## License

OVHBills is released under the MIT license. See the bundled LICENSE file for details.

## Also see

TelcoDirectCallsOVH which allow to get details about your phone calls from OVH API.
https://gitlab.com/BlueRockTEL/TelcoDirectCallsOVH

## Author  

Cyrille Georgel < cyrille@bluerocktel.com >

Also, at BlueRockTEL (http://bluerocktel.com), We help telecom companies billing efficiently and increasing their cash flow. We also provide them with awesome analytics.
